#include "ChemicalFormula.h"


ChemicalFormula::ChemicalFormula()
{
}

ChemicalFormula::ChemicalFormula(std::string formula) : m_formula(formula)
{
}


ChemicalFormula::~ChemicalFormula()
{
}

const std::string ChemicalFormula::GetFormula() const
{
	return m_formula;
}

bool ChemicalFormula::CheckTwoConsecutiveSmallLetters() const
{
	for (int index = 0; index < m_formula.size() - 1; ++index)
		if (islower(m_formula[index]) && islower(m_formula[index + 1]))
			return false;
	return true;
}

bool ChemicalFormula::CheckIfFirstLetterIsNotSmall() const
{
	if (islower(m_formula.front()))
		return false;
	return true;
}

bool ChemicalFormula::CheckIfSmallLetterIsPrecededOnlyByUpperLetter() const
{
	for (int index = 1; index < m_formula.size(); ++index)
		if (islower(m_formula[index]) && isupper(m_formula[index - 1]) == false)
			return false;
	return true;
}

bool ChemicalFormula::CheckIfFirstParenthesisIsOpen() const
{
	//Check if the first parenthesis in formula is "(" NOT ")"
	bool parenthesisFound = false;
	for (const auto& element : m_formula)
	{
		if (element == ')' && parenthesisFound == false)
			return false;
		else
			if (element == '(')
			{
				parenthesisFound = true;
				break;
			}
	}
	return true;
}

bool ChemicalFormula::CheckGoodOpeningAndClosingParenthesis() const
{
	//Check if an opening pharenthesis has a closing paranthesis associated.
	unsigned int currentNumberOfOpenedPharenthesis = 0;
	unsigned int currentNumberOfClosedPharenthesis = 0;
	unsigned int totalNumberOfOpenedPharenthesis = 0;
	unsigned int totalNumberOfClosedPharenthesis = 0;

	for (const auto& element : m_formula)
	{
		if (element == '(')
		{
			currentNumberOfOpenedPharenthesis++;
			totalNumberOfOpenedPharenthesis++;
		}
		else
			if (element == ')')
			{
				if (currentNumberOfOpenedPharenthesis == 0)
					return false;
				totalNumberOfClosedPharenthesis++;
				currentNumberOfOpenedPharenthesis--;
			}
	}

	if (totalNumberOfClosedPharenthesis != totalNumberOfOpenedPharenthesis)
		return false;

	return true;
}

bool ChemicalFormula::CheckIfAfterClosingParenthesisIsANumber() const
{
	for (int index = 0; index < m_formula.size() - 1; ++index)
		if (m_formula[index] == ')' && isdigit(m_formula[index + 1]) == false)
			return false;
	if (m_formula[m_formula.size() - 1] == ')')
		return false;
	return true;
}

bool ChemicalFormula::CheckBetweenParenthesis() const
{
	//Check if between opening and closing pharanthesis exists at least two chemical elements.
	//A chemical formula shouldn't look like this H2(Fe)4. It should have at least 2 chemical elements between paranthesis.
	bool openedParenthesis = false;
	bool closedParenthesis = false;
	unsigned int numberOfElementsBetweenParenthesis = 0;

	if (CheckTwoConsecutiveSmallLetters() && CheckIfFirstLetterIsNotSmall() &&
		CheckIfSmallLetterIsPrecededOnlyByUpperLetter() && CheckGoodOpeningAndClosingParenthesis() &&
		CheckIfFirstParenthesisIsOpen() && CheckIfAfterClosingParenthesisIsANumber())
	{
		for (int index = 0; index < m_formula.size(); ++index)
		{
			if (m_formula[index] == '(')
			{
				openedParenthesis = true;
				numberOfElementsBetweenParenthesis = 0;
				if (isdigit(m_formula[index + 1]))
					return false;
			}
			else
			{
				if (openedParenthesis == true)
				{
					if (m_formula[index] == ')' && (numberOfElementsBetweenParenthesis == 1 || numberOfElementsBetweenParenthesis == 0))
						return false;
					else
					{
						if (isupper(m_formula[index]))
						{
							numberOfElementsBetweenParenthesis++;
							if (islower(m_formula[index + 1]) && isdigit(m_formula[index + 2]))
								index += 2;
							else if (isdigit(m_formula[index + 1]))
								index++;
						}

					}

				}
			}
		}
	}
	return true;
}

bool ChemicalFormula::AllSymbolsAreOk() const
{
	for (const auto& element : m_formula)
		if (element != '(' && element != ')' && isalnum(element) == false)
			return false;
	return true;
}

bool ChemicalFormula::CheckIfFirstCharIsNotDigit() const
{
	if (isdigit(m_formula.front()))
		return false;
	return true;
}

bool ChemicalFormula::TheFormulaIsAllRight() const
{
	if (CheckTwoConsecutiveSmallLetters() && CheckIfSmallLetterIsPrecededOnlyByUpperLetter() &&
		CheckIfFirstParenthesisIsOpen() && CheckIfFirstLetterIsNotSmall() &&
		CheckIfAfterClosingParenthesisIsANumber() && CheckGoodOpeningAndClosingParenthesis() &&
		CheckBetweenParenthesis() && CheckIfFirstCharIsNotDigit() && AllSymbolsAreOk())
		return true;
	return false;
}

void ChemicalFormula::printNumberOfEachElementAtoms()
{
	std::string currentElement;
	bool newElement = true;
	unsigned int numberOfAtomsForBracket = 0;
	unsigned int numberOfAtomsForCurrentElement = 0;
	bool inBrackets = false;
	for (int index = m_formula.size() - 1; index >= 0; index--)
	{
		if (isdigit(m_formula[index]) && m_formula[index - 1] == ')')
		{
			numberOfAtomsForBracket = m_formula[index] - '0';
			inBrackets = true;
		}

		if (inBrackets == true)
		{
			if (isdigit(m_formula[index]) && isupper(m_formula[index - 1]))
			{
				numberOfAtomsForCurrentElement = numberOfAtomsForBracket * (m_formula[index] - '0');
				std::cout << "The number of atoms for element " << m_formula[index - 1] << " is " << numberOfAtomsForCurrentElement << ". " << std::endl;
				index--;
				numberOfAtomsForCurrentElement = 0;
			}

			else {
				if (isdigit(m_formula[index]) && islower(m_formula[index - 1]) && isupper(m_formula[index - 2]))
				{
					numberOfAtomsForCurrentElement = numberOfAtomsForBracket * (m_formula[index] - '0');
					std::cout << "The number of atoms for element " << m_formula[index - 2] << m_formula[index - 1] << " is " << numberOfAtomsForCurrentElement << ". " << std::endl;
					index = index - 2;
					numberOfAtomsForCurrentElement = 0;
				}

				else
				{
					if (islower(m_formula[index]) && isupper(m_formula[index - 1]))
					{
						numberOfAtomsForCurrentElement = numberOfAtomsForBracket;
						std::cout << "The number of atoms for element " << m_formula[index - 1] << m_formula[index] << " is " << numberOfAtomsForCurrentElement << ". " << std::endl;
						index = index - 1;
						numberOfAtomsForCurrentElement = 0;
					}

					else
						if (isupper(m_formula[index]))
							std::cout << "The number of atoms for element " << m_formula[index] << " is " << numberOfAtomsForBracket << ". " << std::endl;
				}
			}
		}

		else
		{
			if (isdigit(m_formula[index]) && isupper(m_formula[index - 1]))
			{
				numberOfAtomsForCurrentElement = m_formula[index] - '0';
				std::cout << "The number of atoms for element " << m_formula[index - 1] << " is " << numberOfAtomsForCurrentElement << ". " << std::endl;
				index--;
				numberOfAtomsForCurrentElement = 0;
			}

			else
			{
				if (isdigit(m_formula[index]) && islower(m_formula[index - 1]) && isupper(m_formula[index - 2]))
				{
					numberOfAtomsForCurrentElement = m_formula[index] - '0';
					std::cout << "The number of atoms for element " << m_formula[index - 2] << m_formula[index - 1] << " is " << numberOfAtomsForCurrentElement << ". " << std::endl;
					index = index - 2;
					numberOfAtomsForCurrentElement = 0;
				}

				else
				{

					if (islower(m_formula[index]) && isupper(m_formula[index - 1]))
					{
						std::cout << "The number of atoms for element " << m_formula[index - 1] << m_formula[index] << " is 1." << std::endl;
						index = index - 1;
						numberOfAtomsForCurrentElement = 0;
					}

					else if (isupper(m_formula[index]))
						std::cout << "The number of atoms for element " << m_formula[index] << " is 1. " << std::endl;

				}
			}

		}

		if (m_formula[index] == '(')
		{
			numberOfAtomsForBracket = 0;
			inBrackets = false;
		}
	}
}

std::istream & operator>>(std::istream & is, ChemicalFormula & formula)
{
	is >> formula.m_formula;
	return is;
}

std::ostream& operator<<(std::ostream& os, const ChemicalFormula& formula)
{
	os << formula.m_formula;
	return os;
}