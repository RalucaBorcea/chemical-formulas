#pragma once
#include <string>
#include <iostream>

class ChemicalFormula
{
public:
	ChemicalFormula();
	ChemicalFormula(std::string formula);
	~ChemicalFormula();

	const std::string GetFormula() const;

	bool CheckTwoConsecutiveSmallLetters() const;
	bool CheckIfFirstLetterIsNotSmall() const;
	bool CheckIfSmallLetterIsPrecededOnlyByUpperLetter() const;
	bool CheckIfFirstParenthesisIsOpen() const;
	bool CheckGoodOpeningAndClosingParenthesis() const;
	bool CheckIfAfterClosingParenthesisIsANumber() const;
	bool CheckBetweenParenthesis() const;
	bool AllSymbolsAreOk() const;
	bool CheckIfFirstCharIsNotDigit() const;
	bool TheFormulaIsAllRight() const;

	void printNumberOfEachElementAtoms();

	friend std::istream& operator >> (std::istream& is, ChemicalFormula& formula);
	friend std::ostream& operator << (std::ostream& os, const ChemicalFormula& formula);

private:
	std::string m_formula;
};

