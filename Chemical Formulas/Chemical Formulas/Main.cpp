#include "ChemicalFormula.h"

int main()
{
	ChemicalFormula myFormula("MgCl2(H2Na)2(CSO4)3Ca5");
	if (myFormula.TheFormulaIsAllRight())
	{
		std::cout << "The number of each element's atoms for formula " << myFormula << " is : " << std::endl;
		myFormula.printNumberOfEachElementAtoms();
	}
	else
		std::cout << "The formula is NOT ok!";
	return 0;
}