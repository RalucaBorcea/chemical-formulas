#include "stdafx.h"
#include "CppUnitTest.h"
#include "ChemicalFormula.h"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace ChemicalFormulasUnitTest
{
	TEST_CLASS(CheckTests)
	{
	public:

		TEST_METHOD(Constructor)
		{
			ChemicalFormula firstFormula("Al2(SO4)3");
			Assert::IsTrue(firstFormula.GetFormula() == "Al2(SO4)3");
		}

		TEST_METHOD(CheckTwoConsecutiveSmallLetters)
		{
			ChemicalFormula myFormula("HCl2C3");
			Assert::IsTrue(myFormula.CheckTwoConsecutiveSmallLetters());
		}

		TEST_METHOD(CheckIfFirstLetterIsNotSmall)
		{
			ChemicalFormula myFormula("H2O");
			Assert::IsTrue(myFormula.CheckIfFirstLetterIsNotSmall());
		}

		TEST_METHOD(CheckIfSmallLetterIsPrecededOnlyByUpperLetter)
		{
			ChemicalFormula myFormula("Al2(SO4)3");
			Assert::IsTrue(myFormula.CheckIfSmallLetterIsPrecededOnlyByUpperLetter());
		}

		TEST_METHOD(CheckIfFirstParenthesisIsOpen)
		{
			ChemicalFormula myFormula("(H2O)2Cl(SO4)2H");
			Assert::IsTrue(myFormula.CheckIfFirstParenthesisIsOpen());
		}

		TEST_METHOD(CheckGoodOpeningAndClosingParenthesis)
		{
			ChemicalFormula myFormula("(H2O)H(CaCl3)2Al2(SO4)3");
			Assert::IsTrue(myFormula.CheckGoodOpeningAndClosingParenthesis());
		}

		TEST_METHOD(CheckIfAfterClosingParenthesisIsANumber)
		{
			ChemicalFormula myFormula("(C8Mg5)2Al2(SO4)2");
			Assert::IsTrue(myFormula.CheckIfAfterClosingParenthesisIsANumber());
		}

		TEST_METHOD(CheckBetweenParenthesis)
		{
			ChemicalFormula myFormula("(H2O)2Al2(HCl2O4)2");
			Assert::IsTrue(myFormula.CheckBetweenParenthesis());
		}

		TEST_METHOD(CheckIfFirstCharIsNotDigit)
		{
			ChemicalFormula myFormula("NaH4Cl");
			Assert::IsTrue(myFormula.CheckIfFirstCharIsNotDigit());
		}

		TEST_METHOD(AllSymbolsAreOk)
		{
			ChemicalFormula myFormula("(H2O)2Al2(HCl2O4)2");
			Assert::IsTrue(myFormula.AllSymbolsAreOk());
		}

		TEST_METHOD(TheFormulaIsAllRight)
		{
			ChemicalFormula myFormula("MgCl2(H2Na)2(CSO4)3Ca5");
			Assert::IsTrue(myFormula.TheFormulaIsAllRight());
		}
	};
}